package com.example.lethanh.firebase

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class ForgotPasswordActivity : AppCompatActivity() {

    var auth: FirebaseAuth ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

       auth = FirebaseAuth.getInstance()

        val btResetPassword = findViewById<Button>(R.id.btn_send_email)
        val edEmail = findViewById<EditText>(R.id.ed_email)
        val progressbar = findViewById<ProgressBar>(R.id.progressBar)

       btResetPassword.setOnClickListener(View.OnClickListener {
           val Email = edEmail.text.toString()
           if (TextUtils.isEmpty(Email)){
               Toast.makeText(applicationContext,"Please input email ",Toast.LENGTH_SHORT).show()
               return@OnClickListener
           }
           progressbar.setVisibility(View.VISIBLE)
           auth!!.sendPasswordResetEmail(Email).addOnCompleteListener {task->
               if (task.isSuccessful()) {
                   startActivity(Intent(this@ForgotPasswordActivity, LoginActivity::class.java))
                   Toast.makeText(this@ForgotPasswordActivity, "We have sent you instructions to reset your password!", Toast.LENGTH_SHORT).show();
               } else {
                   Toast.makeText(this@ForgotPasswordActivity, "Failed to send reset email!", Toast.LENGTH_SHORT).show();
               }
               progressbar.setVisibility(View.GONE);
           }
       })
    }


}
