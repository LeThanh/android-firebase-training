package com.example.lethanh.firebase

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser


class MainActivity : AppCompatActivity() {

    var auth: FirebaseAuth? = null
    var authListener: FirebaseAuth.AuthStateListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        auth = FirebaseAuth.getInstance()

        val user = FirebaseAuth.getInstance().currentUser

        authListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            var user = firebaseAuth.currentUser
            if (user == null){
                startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                finish()
            }
        }

        val SignOut = findViewById<Button>(R.id.btn_sign_out)

        SignOut.setOnClickListener(View.OnClickListener {
            SignOut()
            return@OnClickListener
        })
    }

    fun SignOut() {
        auth!!.signOut()
        startActivity(Intent(this@MainActivity, LoginActivity::class.java))
        finish()
    }


}
