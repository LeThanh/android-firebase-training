package com.example.lethanh.firebase

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth
import android.content.Intent
import android.widget.Toast
import android.text.TextUtils
import android.widget.EditText
import android.widget.ProgressBar




class LoginActivity : AppCompatActivity() {

    private var auth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()
            /*if (auth!=null){
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intent)
            }*/


        val edUser = findViewById<EditText>(R.id.ed_user)
        val edPassWord = findViewById<EditText>(R.id.ed_password)
        val btSignin = findViewById<Button>(R.id.btn_sign_in)
        val btResetPass = findViewById<Button>(R.id.btn_reset_password)
        val btSignUp = findViewById<Button>(R.id.btn_sign_up)
        val progressBar = findViewById<ProgressBar>(R.id.progressBar)

        edUser.background.setAlpha(100);
        edPassWord.background.setAlpha(100);

        btSignin.setOnClickListener(View.OnClickListener {

            val user = edUser.text.toString()
            val pass = edPassWord.text.toString()

            if (TextUtils.isEmpty(user)) {
                Toast.makeText(applicationContext, "Please input email or username", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(pass)) {
                Toast.makeText(applicationContext, "Please input password", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }

            progressBar.setVisibility(View.VISIBLE)

            btSignin.setEnabled(false)
            btSignin.setClickable(false)
            auth!!.signInWithEmailAndPassword(user, pass).addOnCompleteListener { task ->
                if (!task.isSuccessful) {
                    if (pass.length < 6) {
                        edPassWord.setError(getString(R.string.minimum_password))
                    } else {
                        Toast.makeText(this@LoginActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show()
                    }
                    progressBar.setVisibility(View.GONE)
                    btSignin.setEnabled(true)
                    btSignin.setClickable(true)
                }
                else {
                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        })

        btSignUp.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@LoginActivity, SignUpActivity::class.java)
            startActivity(intent)
            return@OnClickListener
        })

        btResetPass.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@LoginActivity,ForgotPasswordActivity::class.java)
            startActivity(intent)
            return@OnClickListener
        })
    }
}
