package com.example.lethanh.firebase

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import android.content.Intent
import android.widget.ProgressBar


class SignUpActivity : AppCompatActivity() {
    private var auth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()

        val edUser = findViewById<EditText>(R.id.ed_user)
        val edPassWord = findViewById<EditText>(R.id.ed_password)
        val btnRegister = findViewById<Button>(R.id.btn_register)
        val progressbar = findViewById<ProgressBar>(R.id.progressBar)

        btnRegister.setOnClickListener(View.OnClickListener {
            val editUser = edUser.text.toString()
            val editPass = edPassWord.text.toString()


            if (TextUtils.isEmpty(editUser)) {
                Toast.makeText(applicationContext, "Enter email", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }

            if (TextUtils.isEmpty(editPass)) {
                Toast.makeText(applicationContext, "Enter password", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }

            if (editPass.length < 6) {
                Toast.makeText(applicationContext, "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }

            progressbar.setVisibility(View.VISIBLE)
            btnRegister.setEnabled(false)
            btnRegister.setClickable(false)
            auth!!.createUserWithEmailAndPassword(editUser, editPass).addOnCompleteListener { task ->

                if (!task.isSuccessful()) {
                    Toast.makeText(this@SignUpActivity, "Authentication failed." + task.getException().toString(),
                            Toast.LENGTH_LONG).show()
                    progressbar.setVisibility(View.GONE)
                    btnRegister.setEnabled(true)
                    btnRegister.setClickable(true)
                } else {
                    startActivity(Intent(this@SignUpActivity, MainActivity::class.java))
                    finish()
                }
            }
        })
    }
}
